<?php 
$username = '';
if (isset($_POST['btnLogin'])) {
	if (isset($_POST['txtUsername']) && !empty($_POST['txtUsername']) && trim($_POST['txtUsername'])) {
		$username =  $_POST['txtUsername'];
	} else {
		$errUsername =  'Enter Username';
	}

	if (isset($_POST['pwdPassword']) && !empty($_POST['pwdPassword'])) {
		$password = $_POST['pwdPassword'];
	} else {
		$errPassword = 'Enter Password';
	}
}
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
</head>
<body>
	<!-- 
		1) Define "action" and "method" attribute inside form opening tag
			Action : Location to send form data into website(Where), default is same page
			Method: Process to send data into website(How)
				- Get-default
				- Post
				- Put/Patch
				- Delete

		2) Name attribute inside form element : Which can be accessed as associative index array

		3) Access form data using following array
			$_GET - if method is get
			$_POST - if method is post
	 -->
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
	<label>Username</label>
	<input type="text" name="txtUsername" value="<?php echo $username ?>">
	<?php if(isset($errUsername)){ echo $errUsername; } ?>
	<br>
	<label>Password</label>
	<input type="password" name="pwdPassword">
	<?php if(isset($errPassword)){ echo $errPassword; } ?>
	<br>
	<input type="submit" name="btnLogin">
</form>
</body>
</html>