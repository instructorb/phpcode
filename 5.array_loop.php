<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP Loop</title>
</head>
<body>
	<pre>
<?php 
$info = [20,'Ram','KTM',60.5,'ram@gmail.com'];
for ($i=0; $i < count($info) ; $i++) { 
	echo $i + 1 . ')' . $info[$i] .'<br>';
}
$info1 = ['sn' => 20,'roll' => 12,'name' => 'Ram Thapa','email' => 'ram@gmail.com','age' => 20,'weight' => 56.5];
?>
<table border="1">
<?php foreach ($info1 as $key => $value) {  ?>
	<tr>
		<th><?php echo ucfirst($key) ?></th>
		<td><?php echo $value ?></td>
	</tr>
<?php } ?>	
</table>
</body>
</html>