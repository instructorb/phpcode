//document object model (DOM) => Manuplate html contents
var doc = document.getElementById('element');
doc.style.color = 'Red';
doc.innerText = "This is my test text";

function green(){
	var para = document.getElementsByClassName('test');
	for (var i = 0; i < para.length; i++) {
		para[i].style.color = 'green';	
	}
}

let red = document.getElementById('red');
red.addEventListener("mouseover",function(event){
	var para = document.getElementsByClassName('test');
	for (var i = 0; i < para.length; i++) {
		para[i].style.color = 'red';	
	}
})