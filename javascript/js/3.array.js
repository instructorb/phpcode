//for array methods: https://www.w3schools.com/jsref/jsref_obj_array.asp

var fruits = ['Apple','Mango','Banana'];
console.log(fruits);
console.log(fruits[0]); //print single data
printData(fruits);

//add into last position
fruits.push('Litchi');
printData(fruits);

//remove from first position
fruits.shift();
printData(fruits);

//add into first position
fruits.unshift('Watermealon');
printData(fruits);
//remove from first position
fruits.pop();
printData(fruits);
function printData(data){
	document.write('*********<br>');
	//print data using loop
	for (var i = 0; i < data.length; i++) {
		document.write(data[i] + '<br>');
	}XMLHttpRequest object is an API for fetching any text base format data, including XML without user/visual interruptions.
All most all browser platform support XMLHttpRequest object to make HTTP requests.

}

/*
Write a JS program to take user input for n number of flower 
and store them into array using function, later print them using loop
*/
