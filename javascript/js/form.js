function validateForm(){

	//reset all error message
	resetError();
	//get all form value using id
	let name= document.getElementById('name').value;
	let phone= document.getElementById('phone').value;
	let email= document.getElementById('email').value;
	let country= document.getElementById('country').selectedIndex;
	let gender= document.reg_form.gender;
	let program= document.reg_form.program;

	// let country= document.getElementById('country').value;

	//set error value 0
	let error = 0;
	//define regex for name
	let namepattern = /^[a-zA-Z\s]+$/;

	let emailpattern = /^([a-zA-Z0-9\.]+\@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/;

	//check blank value for email
	if (email == '') {
		//set error message using span #id
		document.getElementById('err_email').innerHTML = "<b>Enter email</b>";
		error++;
	} else if (!emailpattern.test(email)) {
		document.getElementById('err_email').innerText = "Please enter valid email";
		error++;
	}

	// for phone
	if (phone == '') {
		document.getElementById('err_phone').innerText = "Enter phone";
		error++;
	}

	//for name
	if (name == '') {
		document.getElementById('err_name').innerText = "Enter name";
		error++;
	} else if (name.length < 5) {
		document.getElementById('err_name').innerText = "Minimum 5 character";
		error++;
	} else if (!namepattern.test(name)) {
		document.getElementById('err_name').innerText = "Name must be character and space only";
		error++;
	}

	// for country  : with value
	// if (country == 'Select Country') {
	// 	document.getElementById('err_country').innerText = "Select country";
	// 	error++;
	// }

	//country with selected Index
	if (country == 0) {
		document.getElementById('err_country').innerText = "Select country";
		error++;
	}

	//gender with selected Index
	if (gender[0].checked == false && gender[1].checked == false) {
		document.getElementById('err_gender').innerText = "Select gender";
		error++;
	}

	pchecked = false;
	//program for minimum 1 selection
	for (var i = program.length - 1; i >= 0; i--) {
		if(program[i].checked == true){
			pchecked = true;
		}
	}

	if (pchecked ==  false) {
		document.getElementById('err_program').innerText = "Select any one program";
		error++;
	}


	//count error and return value
	if (error == 0) {
		return true;
	} else {
		return false;
	}
}

function resetError(){
	var errorList = document.getElementsByClassName('error');
	for (var i = 0; i < errorList.length; i++) {
		errorList[i].innerText = '';
	}
}
var country  = document.getElementById('country');
var country_code  = document.getElementById('country_code');

country.addEventListener("change", function(event) {
 country_code.value = country.value;
	 
});