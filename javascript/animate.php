<html>

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/
		jquery/3.3.1/jquery.min.js"></script>
	<script>
		<!-- jQuery code to show animate() method -->
		$(document).ready(function() {
			// $("#b1").click(function() {
			// 	$("#box").animate({
			// 		width: "300px"
			// 	});
			// 	$("#box").animate({
			// 		height: "300px"
			// 	});
			// });
			 $("#b1").click(function() {
                $("#box").animate({
                    height: "200px",
                    width: "200px",
                }, {
                    duration: 2000,
                    easing: "swing",
                    complete: function() {
						$(this).after("<p>Reaches to maximum height and width !</p>");
                    }
                });
            });
		});
	</script>
	<style>
		div {
			width: 100px;
			height: 100px;
			background-color: green;
		}
		
		#b1 {
			margin-top: 10px;
		}
	</style>
</head>

<body>
	<div id="box"></div>
	<!-- click on this button -->
	<button id="b1">Click Here !</button>
</body>

</html>
