<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Event Handler</title>
</head>
<body>
	<h1 id="heading1">Event Listener</h1>
	<button id="me">Click Me</button>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) {
	 	console.log("Hello! I am loaded");
	});
	
	var btn = document.getElementById('me');
	btn.addEventListener('click',function(event){
		var head = document.getElementById('heading1');
		head.style.color='white';
		head.style.backgroundColor='red';
	})
</script>
</body>
</html>