<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>My File</title>
</head>
<body>
	<!-- inline js -->
	<button onclick="alert('test')">Click Me</button>
<!-- Javascript: Can use with html or php
Three types
- Inline : with html tag
- Internal
- External
 -->
 <noscript>
 	Please enable javascript to run your program correctly
 </noscript>
 <!-- Internal -->
<script type="text/javascript">
	alert('test');
</script>

<!-- External -->
<script type="text/javascript" src="js/my.js"></script>
</body>
</html>