<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
<?php 
$students = [
	['roll' => 12,'name' => 'Ram Thapa','email' => 'ram@gmail.com','age' => 20,'weight' => 60.5],
	['roll' => 15,'name' => 'Hari Regmi','email' => 'ram@gmail.com','age' => 20,'weight' => 79.5],
	['roll' => 19,'name' => 'Sita Gurung Thapa','email' => 'ram@gmail.com','age' => 22,'weight' => 35.5]
];
 ?>
 <table border="1">
 	<tr>
 		<th>Roll</th>
 		<th>Name</th>
 		<th>Email</th>
 		<th>Age</th>
 		<th>Weight</th>
 	</tr>
 	<?php foreach ($students as $key => $student) { ?>
 		<tr>
	 		<td><?php echo $student['roll'] ?></td>
	 		<td><?php echo $student['name'] ?></td>
	 		<td><?php echo $student['email'] ?></td>
	 		<td><?php echo $student['age'] ?></td>
	 		<td><?php echo $student['weight'] ?></td>
	 	</tr>
 	<?php } ?>
</body>
</html>