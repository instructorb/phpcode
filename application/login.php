<?php 
require_once 'admin/constant.php';
  $username = $password= "";
  if(isset($_POST['submit'])) {
    $err = [];
    if(isset($_POST['username']) && !empty($_POST['username']) && trim($_POST['username'])) {
      $username = $_POST['username'];
    } else {
      $err['name'] =  "Enter username";
    }

    if(isset($_POST['password']) && !empty($_POST['password'])){
      $password = $_POST['password'];
    } else {
      $err['password'] = "Enter password";
    }
    if (count($err) == 0) {
      //login using database
       try{
          $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
            //query to insert data
          $encpass = md5($password);
          $sql = "select * from users where email='$username' and password='$encpass'";

          $result = $connection->query($sql);
          if ($result->num_rows == 1) {
            $row = $result->fetch_object();

              //start sessoin
            session_start();
            //store data into session
            $_SESSION['user_username'] = $username;
            $_SESSION['user_name'] = $row->name;
            $_SESSION['user_id'] = $row->id;




           //redirect to dashboard
            header('location:dashboard.php');
        } else {
         $error =  'Login failed';
        }
      } catch(Exception $ex){
          die('Database connection Error:' . $ex->getMessage());
      }
    }
  }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Login</title>
  </head>
  <body>
      <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-info">
                  Login Form
                </div>
                <div class="card-body">
                  
                   <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                    <p class="alert alert-danger">Please login to continue</p>
                  <?php } ?>

                  <?php if (isset($error)) { ?>
                    <p class="alert alert-danger"><?php  echo $error; ?></p>
                  <?php } ?>

                  <?php if (isset($success)) { ?>
                    <p class="alert alert-success"><?php  echo $success; ?></p>
                  <?php } ?>
                  <form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form" >
                    <div class="form-group">
                       <label class="form-label">Name</label>
                      <input type = "text" name = "username" value = "<?php echo $username; ?>" class="form-control">
                      <span class="text-danger">
                    <?php if(isset($err['name'])){ echo $err['name']; } ?>
                  </span>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Password</label>
                      <input class="form-control" type = "password" name = "password" value = "<?php echo $password; ?>" > 
                      <span class="text-danger">
                      <?php if(isset($err['password'])){ echo $err['password']; } ?>
                      </span>
                    </div>
                    <div class="form-group mt-2">
                      <input type = "submit" name = "submit" class="btn btn-success" value="Login">
                       <input type = "reset" name = "reset" class="btn btn-danger">
                    </div>
                </form>
              </div>
              </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>