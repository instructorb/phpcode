<?php 
  session_start();
  require_once 'constant.php';
  require_once 'check_session.php';
  $id = $_GET['id'];
    if (!is_numeric($id)) {
      header('location:list_forum.php');
    }
    try{

     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to select  data with specific id
      $sql = "select f.*,c.title as category_title,u.name from forums as f join categories as c on f.category_id=c.id join users as u on f.posted_by=u.id where f.id=$id";
      //exceute query and get result object
      $result = $connection->query($sql);
      if ($result->num_rows > 0) {
       $row = $result->fetch_object();
      } else {
        header('location:list_forum.php');
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <title>Create Forum</title>
  </head>
  <body>
      <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-info">
                  Forum List
                </div>
                 <div class="card-body">
                  <?php if (isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
                   <p class="alert alert-success">Category Deleted Successfully</p>
                 <?php } ?>
                 <?php if (isset($_GET['msg']) && $_GET['msg'] == 2) { ?>
                   <p class="alert alert-success">Category Delete Failed</p>
                 <?php } ?>
                   <table class="table table-bordered">
                    <tr>
                       <th>Category Id</th>
                       <td><?php echo $row->category_title ?></td>
                     </tr>
                     <tr>
                       <th>Title</th>
                       <td><?php echo $row->title ?></td>
                     </tr>
                     <tr>
                       <th>Description</th>
                       <td><?php echo $row->description ?></td>
                     </tr>
                     <tr>
                       <th>Posted date</th>
                       <td><?php echo $row->posted_date ?></td>
                     </tr>
                       <tr>
                       <th>Posted By</th>
                       <td><?php echo $row->name ?></td>
                     </tr>
                   </table>
                </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>