<?php 
  session_start();
  require_once 'constant.php';
  require_once 'check_session.php';
$title = $code = $rank =  '';
  $id = $_GET['id'];
  if (!is_numeric($_GET['id'])) {
    die('Invalid data');
  }
  //fetch old record from database
  try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      //query to insert data
     $sql = "select * from categories where id=$id";

     $result = $connection->query($sql);
     //execute query
     if($result->num_rows == 1){
        $record = $result->fetch_object();
        $title = $record->title;
     } else {
      $msgerror =  'Category not found';
     }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }


if (isset($_POST['btnUpdate'])) {
  $error = [];

  if (isset($_POST['title']) && !empty($_POST['title']) && trim($_POST['title'])) {
    $title = $_POST['title'];
  } else {
    $error['title'] = 'Please enter title';
  }

  if (count($error) == 0) {
    //database connection
    //create database connection object
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      //query to insert data
     $sql = "update categories set title='$title' where id=$id";

     //execute query
     if($connection->query($sql)){
        $msg =  'Category updated successfully';
     }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
    
   
  }
}

 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Update Category</title>
  </head>
  <body>
      <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-info">
                  Category Edit
                </div>
                 <div class="card-body">
                  <?php 
                  if (isset($msg)) { ?>
                   <p class="alert alert-success"><?php  echo $msg; ?></p>
                 <?php }
                   ?>
                     <?php 
                  if (isset($msgerror)) { ?>
                   <p class="alert alert-danger"><?php  echo $msgerror; ?></p>
                 <?php }
                   ?>
                    <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
              <?php require_once 'category_main_form.php' ?>
                        
                        <div class="form-group mt-2">
                          <input type="submit" name="btnUpdate" value="Update" class="btn btn-success">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>