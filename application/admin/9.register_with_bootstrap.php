<?php 
  $username = $address = $email = $phone = $dob =  $country = $gender = $bio = $image = "";
  if(isset($_POST['submit'])) {
    if(isset($_POST['username']) && !empty($_POST['username']) && trim($_POST['username'])) {
      $username = $_POST['username'];
    } else {
      $errname =  "Enter username";
    }

    if(isset($_POST['address']) && !empty($_POST['address']) && trim($_POST['address'])){
      $address = $_POST['address'];
      if ( !preg_match ("/^[a-zA-Z\s]+$/",$address)) {
        $erraddress = "Address must only contain letters!";
      }

    } else {
      $erraddress = "Enter address";
    }

    if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
      $email = $_POST['email'];
    } else {
      $erremail = "Enter email";
    }

    if(isset($_POST['phone']) && !empty($_POST['phone']) && trim($_POST['phone'])){
      $phone = $_POST['phone'];
      if ( !preg_match ("/^977-[0-9]{10}$/",$phone)) {
        $errphone = "Phone pattern error";
      }

    } else{
      $errphone = "Enter phone";
    }

    if(isset($_POST['dob']) && !empty($_POST['dob']) && trim($_POST['dob'])){
      $dob = $_POST['dob'];
    } else {
      $errdob = "Enter date of birth";
    }

    if(isset($_POST['country']) && !empty($_POST['country'])){
      $country = $_POST['country'];
    } else {
      $errcountry = "Select country";
    }

    if(isset($_POST['gender']) && !empty($_POST['gender'])){
      $gender = $_POST['gender'];
    } else {
      $errgender = "Select gender";
    }

    if(isset($_POST['image']) && !empty($_POST['image'])){
      $image = $_POST['image'];
    } else {
      $errimage = "Choose image";
    }

    if(isset($_POST['bio']) && !empty($_POST['bio']) && trim($_POST['bio'])){
      $bio = $_POST['bio'];
    } else {
      $errbio = "Enter bio";
    }
  }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Hello, world!</title>
  </head>
  <body>
    
      <?php require_once "menu.php"; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-info">
                  Registration Form
                </div>
                <div class="card-body">
                  <form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form" >
                    <div class="form-group">
                       <label class="form-label">Name</label>
                      <input type = "text" name = "username" value = "<?php echo $username; ?>" class="form-control">
                    <?php if(isset($errname)){ echo $errname; } ?>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Address</label>
                      <input class="form-control" type = "text" name = "address" value = "<?php echo $address; ?>" > 
                      <span class="text-danger">
                      <?php if(isset($erraddress)){ echo $erraddress; } ?>
                      </span>
                    </div>
                    <div class="form-group">
                      <label class="form-label">Email</label>
                      <input class="form-control" type = "text" name = "email" value = "<?php echo $email; ?>" >
                      <?php if(isset($erremail)){ echo $erremail; } ?>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Phone</label>
                      <input type = "text" name = "phone" value = "<?php echo $phone; ?>" class="form-control" >
                      <?php if(isset($errphone)){ echo $errphone;} ?>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Date Of Birth</label>
                      <input class="form-control" type = "date" name = "dob" value="<?php echo $dob; ?>" >
                      <?php if(isset($errdob)){ echo $errdob; } ?>
                    </div>
                    <div class="form-group mt-2">
                      <label>Country</label>
                      <select name = "country" class="form-control">
                        <option value="">Select Country</option>
                        <option value="np" <?php echo ($country == 'np') ?'selected':'' ?>>Nepal</option>
                        <option value="in" <?php echo ($country == 'in') ?'selected':'' ?>>India</option>
                        <option value="ch" <?php echo ($country == 'ch') ?'selected':'' ?>>China</option>
                      </select>
                      <?php if(isset($errcountry)){ echo $errcountry; } ?>
                    </div>
                    <div class="form-group mt-2">
                    <label class="form-label">Gender</label>
                    <input <?php echo ($gender == "Male")? "checked":''; ?> type = "radio" name = "gender" value = "Male" class="form-check-input">Male
                    <input <?php echo ($gender == "Female") ?"checked":''; ?> type="radio" name="gender" value="Female" class="form-check-input">Female
                    <?php if(isset($errgender)){ echo $errgender;} ?>
                    <br>
                    <div class="form-group mt-2">
                      <label class="form-label">Image</label>
                      <input type = "file" name = "image" class="form-control" > <?php echo $image; ?>
                      <?php if(isset($errimage)){ echo $errimage; } ?>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Bio</label>
                      <textarea name = "bio" class="form-control"><?php echo $bio ?></textarea>
                      <?php if(isset($errbio)){ echo $errbio;} ?>
                    </div>
                    <div class="form-group mt-2">
                      <input type = "submit" name = "submit" class="btn btn-success btn-lg">
                       <input type = "reset" name = "reset" class="btn btn-danger">
                    </div>
                </form>
              </div>
            
              </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>