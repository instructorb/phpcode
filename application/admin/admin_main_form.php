<div class="form-group">
  <label for="name">Name</label>
  <input type="text" name="name" class="form-control" value="<?php echo $name ?>">
  <span class="text-danger">
    <?php echo (isset($error['name'])?$error['name']:'') ?>
  </span>
</div>
<div class="form-group">
  <label for="email">Email</label>
  <input type="text" name="email" class="form-control" value="<?php echo $email ?>">
  <span class="text-danger">
    <?php echo (isset($error['email'])?$error['email']:'') ?>
  </span>
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input type="password" name="password" class="form-control" value="<?php echo $password ?>">
  <span class="text-danger">
    <?php echo (isset($error['password'])?$error['password']:'') ?>
  </span>
</div>