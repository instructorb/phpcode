<div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" value="<?php echo $title ?>">
                <span class="text-danger">
                  <?php echo (isset($error['title'])?$error['title']:'') ?>
                </span>
              </div>
              <div class="form-group">
                <label for="code">Code</label>
                <input type="text" name="code" class="form-control" value="<?php echo $code ?>">
                <span class="text-danger">
                  <?php echo (isset($error['code'])?$error['code']:'') ?>
                </span>
              </div>
              <div class="form-group">
                <label for="rank">Rank</label>
                <input type="text" name="rank" class="form-control" value="<?php echo $rank ?>">
                <span class="text-danger">
                  <?php echo (isset($error['rank'])?$error['rank']:'') ?>
                </span>
              </div>
              <div class="form-group">
                <label for="title">Status</label>
                <input type="radio" name="status" value="1">Active
                <input type="radio" name="status" value="0" checked>De-Active
              </div>