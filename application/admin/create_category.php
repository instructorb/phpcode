<?php 
session_start();
require_once 'constant.php';
require_once 'check_session.php';

$title = $code = $rank =  '';
if (isset($_POST['btnSave'])) {
  $error = [];

  if (isset($_POST['title']) && !empty($_POST['title']) && trim($_POST['title'])) {
    $title = $_POST['title'];
  } else {
    $error['title'] = 'Please enter title';
  }

  if (isset($_POST['code']) && !empty($_POST['code']) && trim($_POST['code'])) {
    $code = $_POST['code'];
  } else {
    $error['code'] = 'Please enter code';
  }

  if (isset($_POST['rank']) && !empty($_POST['rank']) && trim($_POST['rank'])) {
    $rank = $_POST['rank'];
    if (!is_numeric($_POST['rank'])) {
      $error['rank'] = 'Please enter valid rank';
      
    } else if ($_POST['rank'] <= 0) {
      $error['rank'] = 'Please enter positive rank';
    }
  } else {
    $error['rank'] = 'Please enter rank';
  }
  $status = $_POST['status'];

  if (count($error) == 0) {
    //database connection
    //create database connection object
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      //query to insert data
      $sql = "insert into categories(title,code,rank,status) values ('$title','$code',$rank,$status)";

     //execute query
      if($connection->query($sql)){
        $msg =  'Category added successfully';
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
    

  }

}

?>
<!doctype html>
  <html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Create Category</title>
  </head>
  <body>
    <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-info">
              Category Create
            </div>
            <div class="card-body">
              <?php 
              if (isset($msg)) { ?>
               <p class="alert alert-success"><?php  echo $msg; ?></p>
             <?php }
             ?>
             <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
              <?php require_once 'category_main_form.php' ?>
              <div class="form-group mt-2">
                <input type="submit" value="Save" name="btnSave" class="btn btn-success">
              </div>
            </form>
          </div>
          <div class="card-footer">
            This is info
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
  -->
</body>
</html>