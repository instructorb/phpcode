<?php 
session_start();
$id = $_GET['id'];
require_once 'constant.php';
require_once 'check_session.php';
date_default_timezone_set('Asia/Kathmandu');
$title = $description = $image = $category_id =   '';
$status = 0;
try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select id,title from categories";
      //exceute query and get result object
      $result = $connection->query($sql);
      $cats = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($cats, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }


if (isset($_POST['btnUpdate'])) {
  $error = [];

  if (isset($_POST['title']) && !empty($_POST['title']) && trim($_POST['title'])) {
    $title = $_POST['title'];
  } else {
    $error['title'] = 'Please enter title';
  }

  if (isset($_POST['category_id']) && !empty($_POST['category_id']) && trim($_POST['category_id'])) {
    $category_id = $_POST['category_id'];
  } else {
    $error['category_id'] = 'Please enter category_id';
  }

  if (isset($_POST['description']) && !empty($_POST['description']) && trim($_POST['description'])) {
    $description = $_POST['description'];
  } else {
    $error['description'] = 'Please enter description';
  }

  if (isset($_POST['image']) && !empty($_POST['image']) && trim($_POST['image'])) {
    $image = $_POST['image'];
  } else {
    $error['image'] = 'Please enter image';
  }
  $status = $_POST['status'];
 

  if (count($error) == 0) {
    //database connection
    //create database connection object
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      //query to insert data
      $sql = "update forums set title='$title',description='$description',category_id=$category_id,image='$image',status=$status where id=$id";

     //execute query
      if($connection->query($sql)){
        $msg =  'Forum added successfully';
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
    

  }

}


  if (!is_numeric($_GET['id'])) {
    die('Invalid data');
  }
  //fetch old record from database
  try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      //query to insert data
     $sql = "select * from forums where id=$id";

     $result = $connection->query($sql);
     //execute query
     if($result->num_rows == 1){
        $record = $result->fetch_object();
       
        $title = $record->title;
        $category_id = $record->category_id;
        $description = $record->description;
        $image = $record->image;
        $status = $record->status;

     } else {
      $msgerror =  'Forum not found';
     }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }



?>
<!doctype html>
  <html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Edit Forum</title>
  </head>
  <body>
    <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-info">
              Forum Create
            </div>
            <div class="card-body">
              <?php 
              if (isset($msg)) { ?>
               <p class="alert alert-success"><?php  echo $msg; ?></p>
             <?php }
             ?>
             <form accept="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $id ?>" method="post">
              <?php require_once 'forum_main_form.php' ?>
              <div class="form-group mt-2">
                <input type="submit" value="Update" name="btnUpdate" class="btn btn-success">
              </div>
            </form>
          </div>
          <div class="card-footer">
            This is info
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
  -->
</body>
</html>