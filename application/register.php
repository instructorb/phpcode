<?php 
require_once 'admin/constant.php';
$error = [];
  $name = $address = $email = $phone =  $password = "";
  $status=1;
  if(isset($_POST['submit'])) {
    
    if(isset($_POST['name']) && !empty($_POST['name']) && trim($_POST['name'])) {
      $name = $_POST['name'];
    } else {
      $errname =  "Enter name";
    }

    if(isset($_POST['address']) && !empty($_POST['address']) && trim($_POST['address'])){
      $address = $_POST['address'];
      if ( !preg_match ("/^[a-zA-Z\s]+$/",$address)) {
        $erraddress = "Address must only contain letters!";
      }

    } else {
      $erraddress = "Enter address";
    }

    if(isset($_POST['email']) && !empty($_POST['email']) && trim($_POST['email'])){
      $email = $_POST['email'];
    } else {
      $erremail = "Enter email";
    }

    if(isset($_POST['phone']) && !empty($_POST['phone']) && trim($_POST['phone'])){
      $phone = $_POST['phone'];
      if ( !preg_match ("/^977-[0-9]{10}$/",$phone)) {
        $errphone = "Phone pattern error";
      }

    } else{
      $errphone = "Enter phone";
    }
     if(isset($_POST['password']) && !empty($_POST['password']) ) {
      $password = $_POST['password'];
    } else {
      $errpassword =  "Enter password";
    }
 if (count($error) == 0) {
    //database connection
    //create database connection object
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      $encpass = md5($password);
      //query to insert data
      $sql = "insert into users(name,email,password,phone,address,status) values ('$name','$email','$encpass','$phone','$address','$status')";

     //execute query
      if($connection->query($sql)){
        $msg =  'User added successfully';
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
    

  }

}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Hello, world!</title>
  </head>
  <body>
    
    <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-info">
                  Registration Form
                </div>
                <div class="card-body">
                  <?php if (isset($msg)) { ?>
                    <p class="alert alert-success"><?php  echo $msg; ?></p>
                  <?php } ?>
                  <form action= "<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form" >
                    <div class="form-group">
                       <label class="form-label">Name</label>
                      <input type = "text" name = "name" value = "<?php echo $name; ?>" class="form-control">
                    <?php if(isset($errname)){ echo $errname; } ?>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Address</label>
                      <input class="form-control" type = "text" name = "address" value = "<?php echo $address; ?>" > 
                      <span class="text-danger">
                      <?php if(isset($erraddress)){ echo $erraddress; } ?>
                      </span>
                    </div>
                    <div class="form-group">
                      <label class="form-label">Email</label>
                      <input class="form-control" type = "text" name = "email" value = "<?php echo $email; ?>" >
                      <?php if(isset($erremail)){ echo $erremail; } ?>
                    </div>
                    <div class="form-group">
                       <label class="form-label">Password</label>
                      <input type = "password" name = "password" value = "<?php echo $password; ?>" class="form-control">
                    <?php if(isset($errpassword)){ echo $errpassword; } ?>
                    </div>
                    <div class="form-group mt-2">
                      <label class="form-label">Phone</label>
                      <input type = "text" name = "phone" value = "<?php echo $phone; ?>" class="form-control" >
                      <?php if(isset($errphone)){ echo $errphone;} ?>
                    </div>
                    
                    <div class="form-group mt-2">
                      <input type = "submit" name = "submit" class="btn btn-success btn-lg">
                       <input type = "reset" name = "reset" class="btn btn-danger">
                    </div>
                </form>
              </div>
            
              </div>
                <div class="card-footer">
                  This is info
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>