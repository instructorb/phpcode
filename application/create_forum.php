<?php 
session_start();
require_once 'admin/constant.php';
require_once 'check_user_session.php';
date_default_timezone_set('Asia/Kathmandu');
$title = $description = $image = $category_id =  '';
$status = 0;
try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select id,title from categories";
      //exceute query and get result object
      $result = $connection->query($sql);
      $cats = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($cats, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }
if (isset($_POST['btnSave'])) {
  $error = [];

  if (isset($_POST['title']) && !empty($_POST['title']) && trim($_POST['title'])) {
    $title = $_POST['title'];
  } else {
    $error['title'] = 'Please enter title';
  }

  if (isset($_POST['category_id']) && !empty($_POST['category_id']) && trim($_POST['category_id'])) {
    $category_id = $_POST['category_id'];
  } else {
    $error['category_id'] = 'Please enter category_id';
  }

  if (isset($_POST['description']) && !empty($_POST['description']) && trim($_POST['description'])) {
    $description = $_POST['description'];
  } else {
    $error['description'] = 'Please enter description';
  }

  $status = $_POST['status'];
  $posted_by = $_SESSION['user_id'];
  $posted_date = date('Y-m-d H:i:s');
// echo '<pre>';
  //for image
  // print_r($_FILES);
  if (isset($_FILES['image'])) {
      if ($_FILES['image']['error'] == 0) {
          if ($_FILES['image']['size'] <= 500000) {
            $types = ['image/jpeg','image/gif','image/png'];
              if (in_array($_FILES['image']['type'], $types)) {
                
                $image = uniqid() . '_' .$_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], 'admin/images/' . $image);

            } else {
              $error['image'] = 'Please choose valid image type: ' . implode(',', $types);
            } 
          } else {
            $error['image'] = 'Please choose image upto 500kb';
          }
      } else {
        $error['image'] = 'Please select valid image';
      }
  }

  if (count($error) == 0) {
    //database connection
    //create database connection object
    try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

      $description = $connection->real_escape_string($description);
      $title = $connection->real_escape_string($title);

      //query to insert data
     $sql = "insert into forums(title,description,category_id,image,posted_by,posted_date,status) values ('$title','$description',$category_id,'$image',$posted_by,'$posted_date',$status)";

     //execute query
      if($connection->query($sql)){
        $msg =  'Forum added successfully';
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }
    

  }

}

?>
<!doctype html>
  <html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Create Forum</title>
  </head>
  <body>
    <?php require_once 'menu.php'; ?>
    <div class="container mt-5">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-info">
              Forum Create
            </div>
            <div class="card-body">
              <?php 
              if (isset($msg)) { ?>
               <p class="alert alert-success"><?php  echo $msg; ?></p>
             <?php } ?>
             <form accept="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
              <?php require_once 'forum_main_form.php' ?>
              <div class="form-group mt-2">
                <input type="submit" value="Save" name="btnSave" class="btn btn-success">
              </div>
            </form>
          </div>
          <div class="card-footer">
            This is info
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
  -->
</body>
</html>