  <div class="form-group">
    <label for="rank">Category</label>
    <select name="category_id" class="form-control">
        <option value="">Select Category</option>
        <?php foreach($cats as $cat){ ?>
          <option value="<?php echo $cat->id ?>" 
            
            <?php echo ($category_id == $cat->id)?"selected":'' ?>

            ><?php echo $cat->title ?></option>
        <?php } ?>
    </select>
    <span class="text-danger">
      <?php echo (isset($error['category_id'])?$error['category_id']:'') ?>
    </span>
  </div>
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" name="title" class="form-control" value="<?php echo $title ?>">
    <span class="text-danger">
      <?php echo (isset($error['title'])?$error['title']:'') ?>
    </span>
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea  name="description" class="form-control"><?php echo $description ?></textarea>
    <span class="text-danger">
      <?php echo (isset($error['description'])?$error['description']:'') ?>
    </span>
  </div>
  <div class="form-group">
    <label for="image">Image</label>
    <input type="file" name="image" class="form-control" value="<?php echo $image ?>">
    <span class="text-danger">
      <?php echo (isset($error['image'])?$error['image']:'') ?>
    </span>
  </div>
  <div class="form-group">
    <label for="title">Status</label>
    <input type="radio" name="status" value="1" <?php echo ($status == 1)?'checked':'' ?>>Active
    <input type="radio" name="status" value="0" <?php echo ($status == 0)?'checked':'' ?>>De-Active
  </div>