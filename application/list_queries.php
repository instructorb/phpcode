<?php 
  session_start();
  date_default_timezone_set('Asia/Kathmandu');
  require_once 'admin/constant.php';
  require_once 'admin/function.php';

    try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select f.*,c.title as category_title,u.name from forums as f join categories as c on f.category_id=c.id join users as u on f.posted_by=u.id";
      //exceute query and get result object
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
          //add data into array
          array_push($data, $row);
        }
      }
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    }

 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>List Queries</title>
  </head>
  <body>
      <?php require_once 'menu.php'; ?>
    <div class="container-fluid ">
 <h2 class="text-center">List queries</h2>
        <div class="row">
            <div class="col-md-12">
              <div class="album py-5 bg-light">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        <?php foreach($data as $query){ ?>
        <div class="col">
          <div class="card shadow-sm">
            <?php 
            if (!empty($query->image) && file_exists('admin/images/' . $query->image)) { ?>
              <img src="admin/images/<?php echo $query->image ?>" height="246px">
          <?php  } else { ?>
              <img src="images/dummy-post.png">
          <?php  } ?>

            <div class="card-body">
              <p class="card-text"><?php echo $query->title ?>
                <span class="badge rounded-pill bg-primary"><?php echo $query->category_title ?></span>
                <span class="badge rounded-pill bg-success">Posted By:<?php echo $query->name ?></span>

              </p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="view_forum.php?id=<?php echo $query->id ?>" class="btn btn-sm btn-outline-success">View</a>
                 
                </div>
                <small class="text-muted">
                  <?php 
                     echo getPostedTime($query->posted_date);
                   ?>
                </small>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
      </div>
    </div>
  </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>