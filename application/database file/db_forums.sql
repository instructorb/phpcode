-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 06, 2022 at 03:42 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_forums`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`) VALUES
(1, 'Admin', 'admin@gmail.com', '0192023a7bbd73250516f069df18b500'),
(2, 'Hari Bahadur', 'hari@gmail.com', '0769e56eb5d72039f01530d705e971da'),
(3, 'Raj Bahadur Thapa', 'raj@gmail.com', 'affa9d4fb7eccf90ec4f092d2049bcd7');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `rank` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `code`, `rank`, `status`) VALUES
(1, 'Health', 'Modi provident aute', 32, 0),
(2, 'Technology', 'Facere est sit inve', 1, 0),
(3, 'Education', 'Cillum laboriosam r', 787, 1),
(4, 'Foods', 'foods', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `forums`
--

CREATE TABLE `forums` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `posted_by` int(11) NOT NULL,
  `posted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forums`
--

INSERT INTO `forums` (`id`, `category_id`, `title`, `description`, `image`, `status`, `posted_by`, `posted_date`) VALUES
(1, 2, 'This is test1', 'test1', 'test1.jpg', 1, 1, '2022-04-04 17:38:01'),
(5, 2, 'Testtest', 'testtest', 'hair ad.jpg', 1, 1, '2022-04-05 17:25:30'),
(6, 1, 'hi', 'hi', 'KCRVJFVYN5DQNOKI5YFY4PGAXE.jpg', 1, 1, '2022-04-05 17:26:02'),
(7, 1, 'tes', 'teststet', '624c2b4c53e6e_hair ad.jpg', 1, 1, '2022-04-05 17:28:08'),
(8, 1, 'tes', 'teststet', '624c2b557b92d_hair ad.jpg', 1, 1, '2022-04-05 17:28:17'),
(9, 3, 'test', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\nWhy do we use it?\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '624c2c6de9b7c_KCRVJFVYN5DQNOKI5YFY4PGAXE.jpg', 1, 1, '2022-04-05 17:32:57'),
(10, 2, 'Sint magni tempor ve', 'Ut harum odit aliqui', '624d7ad778adf_KCRVJFVYN5DQNOKI5YFY4PGAXE.jpg', 1, 5, '2022-04-06 17:19:47'),
(11, 2, 'Sint magni tempor ve', 'Ut harum odit aliqui', '624d7aecb903f_KCRVJFVYN5DQNOKI5YFY4PGAXE.jpg', 1, 5, '2022-04-06 17:20:08'),
(12, 3, 'Suscipit sint et et', 'Provident ipsum con', '624d806ab86f0_scean.jpg', 1, 1, '2022-04-06 17:43:34'),
(13, 2, 'Quae voluptatem Sim', 'Magna quasi soluta q', '624d80bc572f9_Russian_passport_photo.jpg', 1, 1, '2022-04-06 17:44:56'),
(14, 4, 'Quae est aut necess', 'Aut enim mollitia et', '624d89cb3e337_pimple face.jpg', 1, 1, '2022-04-06 18:23:35');

-- --------------------------------------------------------

--
-- Table structure for table `forum_replies`
--

CREATE TABLE `forum_replies` (
  `id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `reply` text NOT NULL,
  `no_of_dislike` int(11) DEFAULT 0,
  `no_of_like` int(11) DEFAULT 0,
  `reply_by` int(11) NOT NULL,
  `reply_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `address`, `status`) VALUES
(1, 'Rajesh', 'rajesh@gmail.com', 'bf44e33d9745e04551770c7a5a6cdb3b', 987545454, NULL, 1),
(5, 'Kenneth Nunez', 'hi@gmail.com', 'bf44e33d9745e04551770c7a5a6cdb3b', 977, 'Ut incididunt nihil ', 1),
(6, 'Cheyenne Andrews', 'nakak@mailinator.com', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 977, 'Proident repellendu', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `forums`
--
ALTER TABLE `forums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `posted_by` (`posted_by`);

--
-- Indexes for table `forum_replies`
--
ALTER TABLE `forum_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_id` (`forum_id`),
  ADD KEY `reply_by` (`reply_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `forums`
--
ALTER TABLE `forums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `forum_replies`
--
ALTER TABLE `forum_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `forums`
--
ALTER TABLE `forums`
  ADD CONSTRAINT `forums_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `forums_ibfk_2` FOREIGN KEY (`posted_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `forum_replies`
--
ALTER TABLE `forum_replies`
  ADD CONSTRAINT `forum_replies_ibfk_1` FOREIGN KEY (`forum_id`) REFERENCES `forums` (`id`),
  ADD CONSTRAINT `forum_replies_ibfk_2` FOREIGN KEY (`reply_by`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
