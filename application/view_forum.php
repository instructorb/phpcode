<?php 
  session_start();
  date_default_timezone_set('Asia/Kathmandu');
  require_once 'admin/constant.php';
  require_once 'admin/function.php';

  
$id = $_GET['id'];
    try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select f.*,c.title as category_title,u.name from forums as f join categories as c on f.category_id=c.id join users as u on f.posted_by=u.id where f.id=$id";
      //exceute query and get result object
      $result = $connection->query($sql);
      $data = [];
      if ($result->num_rows > 0) {
        $row = $result->fetch_object();
      } else {
        header('location:list_queries.php');
      }
      $connection->close();
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    } 


if (isset($_POST['reply'])) {
    $reply = $_POST['reply'];
    $forum_id = $_GET['id'];
    $reply_by =  $_SESSION['user_id'];
    $reply_date =  date('Y-m-d H:i:s');
    
  try{
      $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      $reply = $connection->real_escape_string($reply);
      //query to insert data
     $sql = "insert into forum_replies(forum_id,reply_by,reply,reply_date) values ('$forum_id','$reply_by','$reply','$reply_date')";

     //execute query
      if($connection->query($sql)){
        $msg =  'Replied  successfully';
        header("location:view_forum.php?id=$id#reply");
      }
    } catch(Exception $ex){
      die('Database connection Error:' . $ex->getMessage());
    }

}


try{
     $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
      //query to insert data
      $sql = "select fr.*,u.name from forum_replies as fr join users as u on fr.reply_by=u.id where fr.forum_id=$id order by fr.reply_date desc";
      //exceute query and get result object
      $result = $connection->query($sql);
      $replies = [];
      if ($result->num_rows > 0) {
        while ($rr = $result->fetch_object()) {
          array_push($replies, $rr);
        }
      } 
      $connection->close();
    } catch(Exception $ex){
        die('Database connection Error:' . $ex->getMessage());
    } 

 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <title>Forum details</title>
  </head>
  <body>
      <?php require_once 'menu.php'; ?>
    <div class="container-fluid ">
 <h2 class="text-center">Forum Details</h2>
        <div class="row">
            <div class="col-md-12">
              <div class="album py-5 bg-light">
    <div class="container">

    <div class="col-md-12">
      

      <article class="blog-post">
        <h2 class="blog-post-title"><?php echo $row->title ?></h2>
        <p class="blog-post-meta"><?php echo $row->posted_date ?> by <a href="#"><?php echo $row->name ?></a></p>

       
        <hr>
        <p style="text-align: justify;">
          <?php echo $row->description; ?>


        </p>
        <div class="mx-auto">
           <img  class="mx-auto" width="100%" src="admin/images/<?php echo $row->image ?>">
        </div>
        <h3 id="reply">Replies</h3>   
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $row->id ?>" method="post">
          <div class="mb-3">
            <textarea name="reply" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="write your response here"></textarea>
          </div>
          <div class="mb-3">
            <button type="submit" name="btnPost" class="btn btn-success">Post</button>
          </div>
          </form>

          <?php foreach($replies as $reply){ ?>
          <div class="mt-2">  
            <p class="blog-post-meta"><?php echo $reply->reply_date ?> by <a href="#"><?php echo $reply->name ?></a></p>
            <p><?php echo $reply->reply ?></p>
            <p>
              <a href="like.php?frid=<?php echo $reply->id ?>&like=<?php echo $reply->no_of_like ?>&id=<?php echo $reply->forum_id ?>"> <?php echo $reply->no_of_like ?>Like</a>
            </p>
            <hr>
          </div>
        
          <?php } ?>
    </div>
    </div>
  </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>