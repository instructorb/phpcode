<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Check Data Type</title>
</head>
<body>
<?php 
/*
if(condition){
	//statement if condition is true
} else {
	//statement if condition is false
}
*/
$name = 'Ram Thapa';

if (is_string($name)) {
	echo 'My name is ' . $name;
}
unset($name);
 ?>
</body>
</html>