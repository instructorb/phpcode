<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<pre>
<?php 
/*
Variable : Memory location to store value at runtime
Rules to create variable in php
- Start with dollor sign '$'
- Followed by character a to z or '_'
- We do not need data type for php variable

Valid : $name,$name1,$_name,$firstname,$first_name,$firstName,$FirstName , $$name

Invalid : $1name,$first name,$1name_first,$first+name,$first-name
*/
//create variable and store/assign value
$name = 'Ram Thapa';
$address = 'Kathmandu';
echo $name;
echo '<br>';
// var_dump function display data with datatype in php
var_dump($name);
echo '<br>';
echo 'My name is ' . $name;
echo '<br>My name is $name' ;
echo "<br>My name is  $name";


$a = 'hi';
$hi = 'Hi, Welcome to website';
echo $$a;
 ?>
 <table border="1" width="30%">
 	<tr>
 		<th>Name</th>
 		<td><?php echo $name ?></td>
 	</tr>
 	<tr>
 		<th>Address</th>
 		<td><?php echo $address; ?></td>
 	</tr>
 </table>
</body>
</html>