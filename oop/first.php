<?php 
/*
inheritence
polymorphisam
abstraction
encapsulation
class
object
method
attributes
interface
final
access modifier
constructor/destructor
*/
/**
 * class classname{
 * 		//attribute/properties/fields
 * 		//method/behaviour/function
 * }
 */
class Book{
	var $name,$no_of_page,$price;

	function setName($n){
		$this->name = $n;
	}

	function setNoOfPage($nop){
		$this->no_of_page = $nop;
	}

	function setPrice($p){
		$this->price = $p;
	}

	function printBookInfo(){
		echo "<br>Book  $this->name consists of $this->no_of_page page and cost Rs. $this->price";
	}

	function getName(){
		return $this->name;
	}

	function getNoOfPage(){
		return $this->no_of_page;
	}

	function getPrice(){
		return $this->price;
	}
}
//objectname = new classname();
$book1  = new Book();
$book1->setName('Programming with PHP');
$book1->setNoOfPage(65);
$book1->setPrice(500);
$book1->printBookInfo();

$book2 = new Book();
$book2->setName('Programming with Java');
$book2->setNoOfPage(655);
$book2->setPrice(1200);
$book2->printBookInfo();
 
 /* Write a PHP program to create account class with customer name,account type, balance. also create method to withdraw and deposite money into given account*/

 ?>