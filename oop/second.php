<?php 
/*class Person{
	var $name,$email,$address,$phone;

	function setValue($var,$value){
		$this->$var = $value;
	}

	function getValue($var){
		return $this->$var;
	}

	function __construct($n,$a,$e,$p){
		$this->name = $n;
		$this->email = $e;
		$this->address = $a;
		$this->phone = $p;
	}

	function getPersonData(){
		return get_object_vars($this);
	}
}

$ram = new Person('Ram Kumar Thapa','KTM','ram@gmail.com',985454545);
$d = $ram->getPersonData();
print_r($d);
$hari = new Person('Hari Kumar Thapa','KTM','hari@gmail.com',9865656565);
$e = $hari->getPersonData();
print_r($e);
*/
/*
Write a PHP program to create object of student object with properties (roll,name,email,course,phone and address), please use constructor to set value for an object.
*/
class Student{
	var $roll,$course,$name,$email,$address,$phone;

	function setValue($var,$value){
		$this->$var = $value;
	}

	function getValue($var){
		return $this->$var;
	}

	function __construct($n,$a,$e,$p,$r,$c){
		$this->name = $n;
		$this->email = $e;
		$this->address = $a;
		$this->phone = $p;
		$this->roll = $r;
		$this->course = $c;
	}

	function getPersonData(){
		return get_object_vars($this);
	}
}
$ram = new Student('Ram Kumar Thapa','KTM','ram@gmail.com',985454545,12,'PHP');
$d = $ram->getPersonData();
print_r($d);
 ?>