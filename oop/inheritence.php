<?php 
class Person{
	private $name,$email,$address,$phone;

	 function setValue($var,$value){
		$this->$var = $value;
	}

	function getValue($var){
		return $this->$var;
	}

	function __construct($n,$a,$e,$p){
		$this->name = $n;
		$this->email = $e;
		$this->address = $a;
		$this->phone = $p;
	}

	final function getData(){
		return get_object_vars($this);
	}
}

class Student extends Person{
	var $roll,$course;

	function __construct($n,$a,$e,$p,$r,$c){

		parent::__construct($n,$a,$e,$p);
		$this->roll = $r;
		$this->course = $c;
	}

	function getData(){
		return get_object_vars($this);
	}
}
$ram = new Student('Ram Kumar Thapa','KTM','ram@gmail.com',985454545,12,'PHP');
// $ram->setValue();
		
$d = $ram->getData();
print_r($d);
 ?>