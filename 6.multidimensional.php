<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<pre>
<?php 
$students = [
	['roll' => 12,'name' => 'Ram Thapa','email' => 'ram@gmail.com','age' => 20,'weight' => 60.5],
	['roll' => 15,'name' => 'Hari Regmi','email' => 'ram@gmail.com','age' => 20,'weight' => 79.5],
	['roll' => 19,'name' => 'Sita Gurung Thapa','email' => 'ram@gmail.com','age' => 22,'weight' => 35.5]
];
 ?>
 <table border="1">
 	<tr>
 		<th>Roll</th>
 		<th>Name</th>
 		<th>Email</th>
 		<th>Age</th>
 		<th>Weight</th>
 	</tr>
 	<?php for ($i=0; $i <count($students) ; $i++) {  ?>
 			<tr>
		 		<td><?php echo $students[$i]['roll'] ?></td>
		 		<td><?php echo $students[$i]['name'] ?></td>
		 		<td><?php echo $students[$i]['email'] ?></td>
		 		<td><?php echo $students[$i]['age'] ?></td>
		 		<td><?php echo $students[$i]['weight'] ?></td>
		 	</tr>
 	<?php } ?>
 </table>
</body>
</html>